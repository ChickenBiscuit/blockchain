package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/ChickenBiscuit/blockchain/controllers"
	hp "gitlab.com/ChickenBiscuit/blockchain/pkg/http"
	"gitlab.com/ChickenBiscuit/blockchain/pkg/logging"

	"github.com/justinas/alice"
	"github.com/rs/cors"
)

func init() {
	logging.DebugEnabled = true
}

func main() {
	// Init servers
	d := hp.New()

	basicMiddleware := alice.New()

	d.HandleFunc("/", controllers.GetBlockChain).Methods(http.MethodGet)
	d.HandleFunc("/", controllers.WriteBlockChain).Methods(http.MethodPost)

	// Allow graceful exit by listening for terminate signal
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)

	corsHandler := cors.New(cors.Options{
		AllowedOrigins: []string{"*"}, // you service is available and allowed for this base url
		AllowedMethods: []string{
			http.MethodGet, // http methods for your app
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions,
			http.MethodHead,
		},
		AllowedHeaders: []string{
			"*", // or you can your header key values which you are using in your application
		},
	})

	server := &http.Server{
		Addr:    ":80",
		Handler: basicMiddleware.Then(corsHandler.Handler(d))} // set listen port
	logging.Info("Starting service...")

	// Use a go function to listen and serve and respond to a shutdown
	canExit := false
	go func() {
		if err := server.ListenAndServe(); err != http.ErrServerClosed {
			logging.Info(err)
		} else {
			logging.Info("Server gracefully stopped")
		}
		canExit = true
	}()

CronLoop:
	for {
		time.Sleep(time.Second) // sleep for 1 second
		select {
		case <-stop:
			{
				logging.Info("Server is being shut down...")
				ctx, cancelFn := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancelFn()
				server.Shutdown(ctx)
			}

		default:
			if canExit {
				logging.Info("Server has been shut down")
				break CronLoop
			}
		}
	}

	// Finish gracefully
	logging.Info("Exiting...")
	os.Exit(0)
}
