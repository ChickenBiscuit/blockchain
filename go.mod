module gitlab.com/ChickenBiscuit/blockchain

go 1.14

require (
	git.dochq.co.uk/packages/logging v0.0.0-20200522165624-3d7c1cd99349 // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/justinas/alice v1.2.0
	github.com/kylelemons/godebug v1.1.0
	github.com/rs/cors v1.7.0
	github.com/ugorji/go/codec v1.1.7
)
