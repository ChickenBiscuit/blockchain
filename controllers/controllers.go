package controllers

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/ChickenBiscuit/blockchain/data"
	"gitlab.com/ChickenBiscuit/blockchain/models"
	hp "gitlab.com/ChickenBiscuit/blockchain/pkg/http"
	"gitlab.com/ChickenBiscuit/blockchain/pkg/logging"
)

var Blockchain []models.Block

func init() {
	t := time.Now()

	genBlock := models.Block{}
	genBlock = models.Block{0, t.String(), "", data.CalculateHash(genBlock), ""}

	Blockchain = append(Blockchain, genBlock)
}

func GetBlockChain(w http.ResponseWriter, r *http.Request) {
	hp.RespondOk(w, r, Blockchain)
}

func WriteBlockChain(w http.ResponseWriter, r *http.Request) {
	var in Message

	if err := json.NewDecoder(r.Body).Decode(&in); err != nil {
		hp.RespondError(w, r, http.StatusInternalServerError, err.Error())
		return
	}

	defer r.Body.Close()

	new, err := data.GenerateBlock(Blockchain[len(Blockchain)-1], in.Data)
	if err != nil {
		hp.RespondError(w, r, http.StatusInternalServerError, err.Error())
		return
	}

	if data.IsBlockValid(new, Blockchain[len(Blockchain)-1]) {
		newChain := append(Blockchain, new)
		data.ReplaceChain(newChain, &Blockchain)
		logging.Info(Blockchain)
	}

	hp.Respond(w, r, http.StatusCreated, new)
}

type Message struct {
	Data string `json:"data"`
}
