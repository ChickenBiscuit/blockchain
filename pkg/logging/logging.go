package logging

import (
	// TODO Copy default format and just remove import
	"fmt"
	"os"
)

var (
	LevelSending Level = 2
	DebugEnabled bool  = false

	Logger LoggerInterface = &TextLogger{}
)

const (
	// DebugLevel level. Usually only enabled when debugging. Very verbose logging.
	DEBUG = 0
	// InfoLevel level. General operational entries about what's going on inside the
	// application.
	INFO = 1
	// WarnLevel level. Non-critical entries that deserve eyes.
	WARN = 2
	// ErrorLevel level. Logs. Used for errors that should definitely be noted.
	// Commonly used for hooks to send errors to an error tracking service.
	ERROR = 3
	// FatalLevel level. Logs and then calls `logger.Exit(1)`. It will exit even if the
	// logging level is set to Panic.
	FATAL = 4
)

type Level uint32

// Some new stuff
// The default text logger

// The logger interface implements a single function to handle the desired response
// for the output
type LoggerInterface interface {
	Log(interface{}, map[string]interface{}, Level) LoggerInterface
}

// The useual defines
func Error(err interface{}) {
	Logger.Log(err, make(map[string]interface{}, 0), ERROR)
}
func Debug(debug interface{}) {
	Logger.Log(debug, make(map[string]interface{}, 0), DEBUG)
}
func Info(info interface{}) {
	Logger.Log(info, make(map[string]interface{}, 0), INFO)
}
func Warn(warn interface{}) {
	Logger.Log(warn, make(map[string]interface{}, 0), WARN)
}
func Fatal(fatal interface{}) {
	Logger.Log(fatal, make(map[string]interface{}, 0), FATAL)
	os.Exit(1)
}

func Errorf(form string, vars ...interface{}) {
	Logger.Log(fmt.Sprintf(form, vars...), make(map[string]interface{}, 0), ERROR)
}
func Debugf(form string, vars ...interface{}) {
	Logger.Log(fmt.Sprintf(form, vars...), make(map[string]interface{}, 0), DEBUG)
}
func Infof(form string, vars ...interface{}) {
	Logger.Log(fmt.Sprintf(form, vars...), make(map[string]interface{}, 0), INFO)
}
func Warnf(form string, vars ...interface{}) {
	Logger.Log(fmt.Sprintf(form, vars...), make(map[string]interface{}, 0), WARN)
}
func Fatalf(form string, vars ...interface{}) {
	Logger.Log(fmt.Sprintf(form, vars...), make(map[string]interface{}, 0), FATAL)
	os.Exit(1)
}
