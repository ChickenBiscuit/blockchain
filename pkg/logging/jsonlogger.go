package logging

import (
	"encoding/json"
	"fmt"
	"os"
	"time"
)

type JsonLogger struct{}

func (j JsonLogger) Log(i interface{}, fields map[string]interface{}, level Level) JsonLogger {

	levels := make(map[Level]string)
	levels[DEBUG] = "DEBUG"
	levels[INFO] = "INFO"
	levels[WARN] = "WARN"
	levels[ERROR] = "ERROR"
	levels[FATAL] = "FATAL"

	output := &JsonLoggingOutput{
		Level:   levels[level],
		Message: i,
		Fields:  fields,
		Time:    time.Now(),
	}

	if err := json.NewEncoder(os.Stdout).Encode(output); err != nil {
		fmt.Print(err)
		return j
	}

	return j
}

type JsonLoggingOutput struct {
	Level   string
	Message interface{}
	Fields  map[string]interface{}
	Time    time.Time
}
