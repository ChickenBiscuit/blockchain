package logging

import (
	// TODO Copy default format and just remove import
	"log"
	"os"
	"os/exec"
	"reflect"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
)

var terminalSupportsColor bool = false

func init() {

	var outString string = "0"

	// Check weather the current terminal supports colours
	out, _ := exec.Command("/usr/bin/tput", "colors").Output()

	outString = strings.TrimSpace(string(out))
	colours, _ := strconv.Atoi(outString)

	if colours > 8 {
		terminalSupportsColor = true
	}
}

type TextLogger struct{}

func (t TextLogger) Log(i interface{}, fields map[string]interface{}, level Level) LoggerInterface {
	errLog := log.New(os.Stderr, "", 0)
	outLog := log.New(os.Stdout, "", 0)
	var levelToString = func(l Level) string {
		switch l {
		case DEBUG:
			if terminalSupportsColor {
				return " \u001b[32mDEBUG\u001b[0m "
			}
			return " DEBUG "
		case INFO:
			if terminalSupportsColor {
				return " \u001b[34mINFO\u001b[0m  "
			}
			return "INFO  "
		case WARN:
			if terminalSupportsColor {
				return " \u001b[33mWARN\u001b[0m  "
			}
			return " WARN  "
		case ERROR:
			if terminalSupportsColor {
				return " \u001b[31mERROR\u001b[0m "
			}
			return " ERROR "
		case FATAL:
			if terminalSupportsColor {
				return " \u001b[31mFATAL\u001b[0m "
			}
			return " FATAL "
		default:
			return " N/A "
		}
	}
	// Get the runtime caller
	pc, _, line, ok := runtime.Caller(2)
	details := runtime.FuncForPC(pc)
	if !ok && details == nil {
		return t
	}

	// Check the current debug status
	if level == DEBUG && !DebugEnabled {
		return t
	}

	// Manage the current time (if enabled)
	var timestring string
	if DebugEnabled {
		timestring = time.Now().Format("Mon Jan _2 15:04:05") + " "
	}

	// If the type is a structure format it nicly for output
	if t := reflect.TypeOf(i); t.Kind() == reflect.Struct {
		i = "\n" + spew.Sdump(i)
	}

	// Include tracing information if debug mode is enabled
	if DebugEnabled {
		if level <= LevelSending {
			outLog.Printf("%s[%s] [%s#%d] %s \n", timestring, levelToString(level), details.Name(), line, i)
			return t
		} else {
			errLog.Printf("%s[%s] [%s#%d] %s \n", timestring, levelToString(level), details.Name(), line, i)
			return t
		}
	}

	// Kepp the logs simple if debug is not enabled
	if level <= LevelSending {
		outLog.Printf("%s[%s] %s \n", timestring, levelToString(level), i)
		return t
	} else {
		errLog.Printf("%s[%s] %s \n", timestring, levelToString(level), i)
		return t
	}

	return t
}
