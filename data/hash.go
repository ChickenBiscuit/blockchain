package data

import (
	"crypto/sha256"
	"encoding/hex"

	"gitlab.com/ChickenBiscuit/blockchain/models"
)

func CalculateHash(block models.Block) string {
	record := string(block.Index) + block.Timestamp + block.SomeRandomData + block.PrevHash
	h := sha256.New()
	h.Write([]byte(record))
	hashed := h.Sum(nil)

	return hex.EncodeToString(hashed)
}
