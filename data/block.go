package data

import (
	"time"

	"gitlab.com/ChickenBiscuit/blockchain/models"
)

func GenerateBlock(old models.Block, data string) (models.Block, error) {
	var new models.Block

	t := time.Now()

	new.Index = old.Index + 1
	new.Timestamp = t.String()
	new.SomeRandomData = data
	new.PrevHash = old.Hash
	new.Hash = CalculateHash(new)

	return new, nil
}

func IsBlockValid(new models.Block, old models.Block) bool {
	if old.Index+1 != new.Index {
		return false
	}

	if old.Hash != new.PrevHash {
		return false
	}

	if CalculateHash(new) != new.Hash {
		return false
	}

	return true
}

func ReplaceChain(new []models.Block, chain *[]models.Block) {
	if len(new) > len(*chain) {
		*chain = new
	}
}
