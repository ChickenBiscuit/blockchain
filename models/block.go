package models

// Each block contains data that will be written to the blockchain and represents
// each case
type Block struct {
	Index          int    // the position of the data record in the blockchain
	Timestamp      string // the time the data was written
	SomeRandomData string // the data to be saved
	Hash           string // the SHA256 identifier for the record
	PrevHash       string // the SHA265 identifier for the past record in the chain
}

type Blockchain []Block
