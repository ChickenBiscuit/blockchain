GO        ?= go
TAGS      :=
LDFLAGS	  := -w -s
TESTS     := .
TESTFLAGS :=
GOFLAGS   :=
RUNFLAGS  :=
BINDIR    := $(CURDIR)/bin

GOOS := linux
GOARCH := amd64

#
# CI/CD Commands
#
all: server

server:
	CGO_ENABLED=0 GOOS=$(shell uname -s | tr '[:upper:]' '[:lower:]') GOARCH=amd64 $(GO) build -a -ldflags="$(LDFLAGS)" -v ./cmd/main.go

.PHONY: clean
clean:
	@rm -rf ./main
